import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

import './body.html';

Template.body.onCreated(function(){
    this.title = new ReactiveVar('');
});

Template.body.onRendered(function(){
    this.autorun(function(){
        let title = this.title.get();
        if (!_.isEmpty(title))
            alert('Setting up ' + title);
    }.bind(this));
});

Template.body.helpers({
    title: function () {
        return Template.instance().title.get();
    }
});

Template.body.events({
    "click #link1": function (event, template) {
        console.log("click #link1");
        template.title.set("Title 1")
    },

    "click #link2": function (event, template) {
        console.log("click #link2");
        template.title.set("Title 2")
    }

});